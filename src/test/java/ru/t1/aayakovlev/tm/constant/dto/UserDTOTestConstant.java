package ru.t1.aayakovlev.tm.constant.dto;

import ru.t1.aayakovlev.tm.dto.model.UserDTO;

public final class UserDTOTestConstant {

    public final static String USER_LOGIN = "user";

    public final static String USER_PASSWORD = "user";

    public final static String USER_ADMIN_LOGIN = "admin";

    public final static String USER_ADMIN_PASSWORD = "admin";

    public final static UserDTO USER_ONE = new UserDTO();

    public final static UserDTO USER_TWO = new UserDTO();

    public final static UserDTO USER_THREE = new UserDTO();

}
