package ru.t1.aayakovlev.tm.constant.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.model.Project;

public final class ProjectTestConstant {

    @NotNull
    public static final Project PROJECT_ONE = new Project("First");

    @NotNull
    public static final Project PROJECT_TWO = new Project("Second");

    @NotNull
    public static final Project PROJECT_THREE = new Project("Third");

    @NotNull
    public static final Project PROJECT_FOUR = new Project("Fourth");

}
