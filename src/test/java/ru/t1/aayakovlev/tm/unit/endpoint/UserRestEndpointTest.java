package ru.t1.aayakovlev.tm.unit.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import ru.t1.aayakovlev.tm.marker.UnitCategory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.util.NestedServletException;
import ru.t1.aayakovlev.tm.api.service.dto.IUserDTOService;
import ru.t1.aayakovlev.tm.config.ApplicationConfig;
import ru.t1.aayakovlev.tm.config.DatabaseConfig;
import ru.t1.aayakovlev.tm.config.SecurityConfig;
import ru.t1.aayakovlev.tm.config.WebMVCApplicationConfig;
import ru.t1.aayakovlev.tm.dto.model.UserDTO;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.t1.aayakovlev.tm.constant.dto.UserDTOTestConstant.*;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfig.class, DatabaseConfig.class, SecurityConfig.class, WebMVCApplicationConfig.class})
public class UserRestEndpointTest {

    @NotNull
    @Autowired
    private PasswordEncoder passwordEncoder;

    @NotNull
    @Resource
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private WebApplicationContext wac;

    @NotNull
    @Autowired
    private IUserDTOService service;

    @NotNull
    private MockMvc mockMvc;

    @NotNull
    private static final String USERS_URL = "http://localhost:8080/api/users";

    @Before
    public void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USER_ADMIN_LOGIN, USER_ADMIN_LOGIN);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        USER_ONE.setLogin("test_one");
        USER_ONE.setPasswordHash(passwordEncoder.encode("test_one"));
        USER_TWO.setLogin("test_two");
        USER_TWO.setPasswordHash(passwordEncoder.encode("test_two"));
        USER_THREE.setLogin("test_three");
        USER_THREE.setPasswordHash(passwordEncoder.encode("test_three"));
        add(USER_ONE);
        add(USER_TWO);
    }

    @After
    @SneakyThrows
    public void finish() {
        service.deleteById(USER_ONE.getId());
        service.deleteById(USER_TWO.getId());
    }

    @SneakyThrows
    private void add(@NotNull final UserDTO user) {
        @NotNull final String url = USERS_URL + "/save";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(user);
        mockMvc.perform(MockMvcRequestBuilders.put(url)
                .content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Nullable
    @SneakyThrows
    private UserDTO findById(@NotNull final String id) {
        @NotNull final String url = USERS_URL + "/find/" + id;
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        if ("".equals(json)) return null;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, UserDTO.class);
    }

    @Test
    @SneakyThrows
    public void count() {
        @NotNull final String url = USERS_URL + "/count";
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        Assert.assertEquals(4, objectMapper.readValue(json, Long.class).longValue());
    }

    @Test
    @SneakyThrows
    public void deleteById() {
        add(USER_THREE);
        @NotNull final String url = USERS_URL + "/delete/" + USER_THREE.getId();
        mockMvc.perform(MockMvcRequestBuilders.delete(url)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertThrows(
                NestedServletException.class,
                () -> findById(USER_THREE.getId())
        );
    }

    @Test
    @SneakyThrows
    public void existsById() {
        @NotNull final String url = USERS_URL + "/exists/" + USER_ONE.getId();
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        Assert.assertTrue(objectMapper.readValue(json, Boolean.class));
    }

    @Test
    @SneakyThrows
    public void findAll() {
        @NotNull final String url = USERS_URL + "/find";
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @Nullable final List<UserDTO> users = Arrays.asList(objectMapper.readValue(json, UserDTO[].class));
        Assert.assertNotNull(users);
        Assert.assertEquals(4, users.size());
    }

    @Test
    @SneakyThrows
    public void findById() {
        @NotNull final UserDTO user = findById(USER_ONE.getId());
        Assert.assertEquals(USER_ONE.getId(), user.getId());
        Assert.assertEquals(USER_ONE.getLogin(), user.getLogin());
        Assert.assertEquals(USER_ONE.getPasswordHash(), user.getPasswordHash());
    }

    @Test
    @SneakyThrows
    public void save() {
        add(USER_THREE);
        @Nullable final UserDTO user = findById(USER_THREE.getId());
        Assert.assertNotNull(user);
        service.deleteById(USER_THREE.getId());
    }

    @Test
    @SneakyThrows
    public void update() {
        USER_TWO.setLogin("test_two_updated");
        @NotNull final String url = USERS_URL + "/save/" + USER_TWO.getId();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(USER_TWO);
        mockMvc.perform(MockMvcRequestBuilders.post(url)
                .content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        @Nullable final UserDTO user = findById(USER_TWO.getId());
        Assert.assertEquals(user.getLogin(), "test_two_updated");
    }

}
