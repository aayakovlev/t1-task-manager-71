package ru.t1.aayakovlev.tm.unit.controller;

import lombok.SneakyThrows;
import ru.t1.aayakovlev.tm.marker.UnitCategory;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.t1.aayakovlev.tm.api.service.dto.IProjectDTOService;
import ru.t1.aayakovlev.tm.config.ApplicationConfig;
import ru.t1.aayakovlev.tm.config.DatabaseConfig;
import ru.t1.aayakovlev.tm.config.SecurityConfig;
import ru.t1.aayakovlev.tm.config.WebMVCApplicationConfig;
import ru.t1.aayakovlev.tm.dto.model.ProjectDTO;
import ru.t1.aayakovlev.tm.exception.entity.ProjectNotFoundException;
import ru.t1.aayakovlev.tm.util.UserUtil;

import java.util.List;

import static ru.t1.aayakovlev.tm.constant.dto.ProjectDTOTestConstant.*;
import static ru.t1.aayakovlev.tm.constant.dto.UserDTOTestConstant.USER_LOGIN;
import static ru.t1.aayakovlev.tm.constant.dto.UserDTOTestConstant.USER_PASSWORD;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfig.class, DatabaseConfig.class, SecurityConfig.class, WebMVCApplicationConfig.class})
public class ProjectControllerTest {

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private IProjectDTOService service;

    @NotNull
    @Autowired
    private WebApplicationContext wac;

    @NotNull
    private MockMvc mockMvc;

    @Before
    @SneakyThrows
    public void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USER_LOGIN, USER_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        PROJECT_ONE.setUserId(UserUtil.getUserId());
        PROJECT_TWO.setUserId(UserUtil.getUserId());
        PROJECT_THREE.setUserId(UserUtil.getUserId());
        service.save(PROJECT_ONE);
        service.save(PROJECT_TWO);
    }

    @After
    @SneakyThrows
    public void finish() {
        service.deleteAllByUserId(UserUtil.getUserId());
    }

    @Test
    @SneakyThrows
    public void findAll() {
        @NotNull final String url = "/projects";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    public void create() {
        @NotNull final String url = "/projects/create";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        @NotNull final List<ProjectDTO> projects = service.findAllByUserId(UserUtil.getUserId());
        Assert.assertNotNull(projects);
        Assert.assertEquals(3, projects.size());
    }

    @Test
    @SneakyThrows
    public void delete() {
        @NotNull final String url = "/projects/delete/" + PROJECT_ONE.getId();
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        Assert.assertThrows(ProjectNotFoundException.class, () ->
                service.findByUserIdAndId(UserUtil.getUserId(), PROJECT_ONE.getId())
        );
    }

    @Test
    @SneakyThrows
    public void edit() {
        @NotNull final String url = "/projects/edit/" + PROJECT_TWO.getId();
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().isOk());
    }

}
