package ru.t1.aayakovlev.tm.unit.repository.dto;

import ru.t1.aayakovlev.tm.marker.UnitCategory;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aayakovlev.tm.config.ApplicationConfig;
import ru.t1.aayakovlev.tm.config.DatabaseConfig;
import ru.t1.aayakovlev.tm.config.SecurityConfig;
import ru.t1.aayakovlev.tm.dto.model.TaskDTO;
import ru.t1.aayakovlev.tm.repository.dto.ProjectDTORepository;
import ru.t1.aayakovlev.tm.repository.dto.TaskDTORepository;
import ru.t1.aayakovlev.tm.util.UserUtil;

import java.util.List;

import static ru.t1.aayakovlev.tm.constant.dto.ProjectDTOTestConstant.PROJECT_ONE;
import static ru.t1.aayakovlev.tm.constant.dto.TaskDTOTestConstant.TASK_ONE;
import static ru.t1.aayakovlev.tm.constant.dto.TaskDTOTestConstant.TASK_TWO;
import static ru.t1.aayakovlev.tm.constant.dto.UserDTOTestConstant.USER_LOGIN;
import static ru.t1.aayakovlev.tm.constant.dto.UserDTOTestConstant.USER_PASSWORD;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfig.class, DatabaseConfig.class, SecurityConfig.class})
public class TaskDTORepositoryTest {

    @NotNull
    @Autowired
    private TaskDTORepository repository;

    @NotNull
    @Autowired
    private ProjectDTORepository projectRepository;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @Before
    public void init() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USER_LOGIN, USER_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        projectRepository.save(PROJECT_ONE);
        TASK_ONE.setUserId(UserUtil.getUserId());
        TASK_ONE.setProjectId(PROJECT_ONE.getId());
        TASK_TWO.setUserId(UserUtil.getUserId());
        TASK_TWO.setProjectId(PROJECT_ONE.getId());
        repository.save(TASK_ONE);
        repository.save(TASK_TWO);
    }

    @After
    public void finish() {
        repository.deleteAll();
    }

    @Test
    public void count() {
        Assert.assertEquals(2, repository.countByUserId(UserUtil.getUserId()));
    }

    @Test
    @Transactional
    public void deleteAll() {
        repository.deleteAllByUserId(UserUtil.getUserId());
        Assert.assertEquals(0, repository.countByUserId(UserUtil.getUserId()));
    }

    @Test
    @Transactional
    public void deleteById() {
        repository.deleteByUserIdAndId(UserUtil.getUserId(), TASK_ONE.getId());
        Assert.assertNull(
                repository.findByUserIdAndId(UserUtil.getUserId(), TASK_ONE.getId()).orElse(null)
        );
    }

    @Test
    public void existsById() {
        Assert.assertTrue(repository.existsByUserIdAndId(UserUtil.getUserId(), TASK_ONE.getId()));
    }

    @Test
    public void findAll() {
        @NotNull final List<TaskDTO> tasks = repository.findAllByUserId(UserUtil.getUserId());
        Assert.assertEquals(2, tasks.size());
    }

    @Test
    public void findAllByProjectId() {
        @NotNull final List<TaskDTO> tasks = repository.findAllByUserIdAndProjectId(UserUtil.getUserId(), PROJECT_ONE.getId());
        Assert.assertEquals(2, tasks.size());
    }

    @Test
    public void findById() {
        @NotNull final TaskDTO task = repository
                .findByUserIdAndId(UserUtil.getUserId(), TASK_ONE.getId())
                .get();
        Assert.assertEquals(TASK_ONE.getId(), task.getId());
        Assert.assertEquals(TASK_ONE.getName(), task.getName());
        Assert.assertEquals(TASK_ONE.getDescription(), task.getDescription());
        Assert.assertEquals(TASK_ONE.getStatus(), task.getStatus());
        Assert.assertEquals(TASK_ONE.getCreated(), task.getCreated());
        Assert.assertEquals(TASK_ONE.getUserId(), task.getUserId());
    }

}
