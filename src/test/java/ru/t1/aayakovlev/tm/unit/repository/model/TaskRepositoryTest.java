package ru.t1.aayakovlev.tm.unit.repository.model;

import ru.t1.aayakovlev.tm.marker.UnitCategory;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aayakovlev.tm.config.ApplicationConfig;
import ru.t1.aayakovlev.tm.config.DatabaseConfig;
import ru.t1.aayakovlev.tm.config.SecurityConfig;
import ru.t1.aayakovlev.tm.model.Task;
import ru.t1.aayakovlev.tm.model.User;
import ru.t1.aayakovlev.tm.repository.model.ProjectRepository;
import ru.t1.aayakovlev.tm.repository.model.TaskRepository;
import ru.t1.aayakovlev.tm.repository.model.UserRepository;
import ru.t1.aayakovlev.tm.util.UserUtil;

import java.util.List;

import static ru.t1.aayakovlev.tm.constant.model.ProjectTestConstant.PROJECT_ONE;
import static ru.t1.aayakovlev.tm.constant.model.TaskTestConstant.TASK_ONE;
import static ru.t1.aayakovlev.tm.constant.model.TaskTestConstant.TASK_TWO;
import static ru.t1.aayakovlev.tm.constant.dto.UserDTOTestConstant.USER_LOGIN;
import static ru.t1.aayakovlev.tm.constant.dto.UserDTOTestConstant.USER_PASSWORD;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfig.class, DatabaseConfig.class, SecurityConfig.class})
public class TaskRepositoryTest {

    @NotNull
    @Autowired
    private TaskRepository repository;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private UserRepository userRepository;

    @NotNull
    @Autowired
    private ProjectRepository projectRepository;

    @NotNull
    private User user;

    @Before
    public void init() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USER_LOGIN, USER_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        user = userRepository.findById(UserUtil.getUserId()).get();
        projectRepository.save(PROJECT_ONE);
        TASK_ONE.setUser(user);
        TASK_ONE.setProject(PROJECT_ONE);
        TASK_TWO.setUser(user);
        TASK_TWO.setProject(PROJECT_ONE);
        repository.save(TASK_ONE);
        repository.save(TASK_TWO);
    }

    @After
    public void finish() {
        repository.deleteAll();
    }

    @Test
    public void count() {
        Assert.assertEquals(2, repository.countByUser(userRepository.findById(UserUtil.getUserId()).get()));
    }

    @Test
    @Transactional
    public void deleteAll() {
        repository.deleteAllByUser(user);
        Assert.assertEquals(0, repository.countByUser(user));
    }

    @Test
    @Transactional
    public void deleteById() {
        repository.deleteByUserAndId(user, TASK_ONE.getId());
        Assert.assertNull(
                repository.findByUserAndId(user, TASK_ONE.getId()).orElse(null)
        );
    }

    @Test
    public void existsById() {
        Assert.assertTrue(repository.existsByUserAndId(user, TASK_ONE.getId()));
    }

    @Test
    public void findAll() {
        @NotNull final List<Task> tasks = repository.findAllByUser(user);
        Assert.assertEquals(2, tasks.size());
    }

    @Test
    public void findAllByProjectId() {
        @NotNull final List<Task> tasks = repository.findAllByUserAndProjectId(user, PROJECT_ONE.getId());
        Assert.assertEquals(2, tasks.size());
    }

    @Test
    public void findById() {
        @NotNull final Task task = repository
                .findByUserAndId(user, TASK_ONE.getId())
                .get();
        Assert.assertEquals(TASK_ONE.getId(), task.getId());
        Assert.assertEquals(TASK_ONE.getName(), task.getName());
        Assert.assertEquals(TASK_ONE.getDescription(), task.getDescription());
        Assert.assertEquals(TASK_ONE.getStatus(), task.getStatus());
        Assert.assertEquals(TASK_ONE.getCreated(), task.getCreated());
    }

}
