package ru.t1.aayakovlev.tm.unit.service.dto;

import lombok.SneakyThrows;
import ru.t1.aayakovlev.tm.marker.UnitCategory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.t1.aayakovlev.tm.api.service.dto.IUserDTOService;
import ru.t1.aayakovlev.tm.config.ApplicationConfig;
import ru.t1.aayakovlev.tm.config.DatabaseConfig;
import ru.t1.aayakovlev.tm.config.SecurityConfig;
import ru.t1.aayakovlev.tm.dto.model.UserDTO;
import ru.t1.aayakovlev.tm.exception.entity.UserNotFoundException;

import java.util.List;

import static ru.t1.aayakovlev.tm.constant.dto.UserDTOTestConstant.*;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfig.class, DatabaseConfig.class, SecurityConfig.class})
public class UserDTOServiceTest {

    @NotNull
    @Autowired
    private PasswordEncoder passwordEncoder;

    @NotNull
    @Autowired
    private IUserDTOService service;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @Before
    @SneakyThrows
    public void init() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USER_LOGIN, USER_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        USER_ONE.setLogin("test_one");
        USER_ONE.setPasswordHash(passwordEncoder.encode("test_one"));
        USER_TWO.setLogin("test_two");
        USER_TWO.setPasswordHash(passwordEncoder.encode("test_two"));
        service.save(USER_ONE);
        service.save(USER_TWO);
    }

    @After
    @SneakyThrows
    public void finish() {
        service.deleteById(USER_ONE.getId());
        service.deleteById(USER_TWO.getId());
    }

    @Test
    @SneakyThrows
    public void count() {
        Assert.assertEquals(4, service.count());
    }

    @Test
    @SneakyThrows
    public void deleteById() {
        service.save(USER_THREE);
        service.deleteById(USER_THREE.getId());
        Assert.assertThrows(UserNotFoundException.class, () -> service.findById(USER_THREE.getId()));
    }

    @Test
    @SneakyThrows
    public void existsById() {
        Assert.assertTrue(service.existsById(USER_ONE.getId()));
    }

    @Test
    @SneakyThrows
    public void findAll() {
        @NotNull final List<UserDTO> users = service.findAll();
        Assert.assertEquals(4, users.size());
    }

    @Test
    @SneakyThrows
    public void findById() {
        @NotNull final UserDTO user = service
                .findById(USER_ONE.getId());
        Assert.assertEquals(USER_ONE.getId(), user.getId());
        Assert.assertEquals(USER_ONE.getLogin(), user.getLogin());
        Assert.assertEquals(USER_ONE.getPasswordHash(), user.getPasswordHash());
    }

    @Test
    public void findByLogin() {
        @Nullable final UserDTO user = service.findByLogin(USER_TWO.getLogin());
        Assert.assertEquals(USER_TWO.getId(), user.getId());
        Assert.assertEquals(USER_TWO.getLogin(), user.getLogin());
        Assert.assertEquals(USER_TWO.getPasswordHash(), user.getPasswordHash());
    }

}
