package ru.t1.aayakovlev.tm.unit.service.model;

import lombok.SneakyThrows;
import ru.t1.aayakovlev.tm.marker.UnitCategory;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.t1.aayakovlev.tm.api.service.model.IUserService;
import ru.t1.aayakovlev.tm.config.ApplicationConfig;
import ru.t1.aayakovlev.tm.config.DatabaseConfig;
import ru.t1.aayakovlev.tm.config.SecurityConfig;
import ru.t1.aayakovlev.tm.exception.entity.UserNotFoundException;
import ru.t1.aayakovlev.tm.model.User;

import java.util.List;

import static ru.t1.aayakovlev.tm.constant.dto.UserDTOTestConstant.*;
import static ru.t1.aayakovlev.tm.constant.model.UserTestConstant.USER_ONE;
import static ru.t1.aayakovlev.tm.constant.model.UserTestConstant.USER_TWO;
import static ru.t1.aayakovlev.tm.constant.model.UserTestConstant.USER_THREE;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfig.class, DatabaseConfig.class, SecurityConfig.class})
public class UserServiceTest {

    @NotNull
    @Autowired
    private IUserService service;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @Before
    @SneakyThrows
    public void init() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USER_LOGIN, USER_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        service.save(USER_ONE);
        service.save(USER_TWO);
    }

    @After
    @SneakyThrows
    public void finish() {
        service.deleteById(USER_ONE.getId());
        service.deleteById(USER_TWO.getId());
    }

    @Test
    @SneakyThrows
    public void count() {
        Assert.assertEquals(4, service.count());
    }

    @Test
    @SneakyThrows
    public void deleteById() {
        service.save(USER_THREE);
        service.deleteById(USER_THREE.getId());
        Assert.assertThrows(UserNotFoundException.class, () -> service.findById(USER_THREE.getId()));
    }

    @Test
    @SneakyThrows
    public void existsById() {
        Assert.assertTrue(service.existsById(USER_ONE.getId()));
    }

    @Test
    @SneakyThrows
    public void findAll() {
        @NotNull final List<User> users = service.findAll();
        Assert.assertEquals(4, users.size());
    }

    @Test
    @SneakyThrows
    public void findById() {
        @NotNull final User user = service.findById(USER_ONE.getId());
        Assert.assertEquals(USER_ONE.getId(), user.getId());
        Assert.assertEquals(USER_ONE.getLogin(), user.getLogin());
        Assert.assertEquals(USER_ONE.getPasswordHash(), user.getPasswordHash());
    }

}
