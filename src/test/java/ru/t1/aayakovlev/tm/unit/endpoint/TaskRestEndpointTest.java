package ru.t1.aayakovlev.tm.unit.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import ru.t1.aayakovlev.tm.marker.UnitCategory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.util.NestedServletException;
import ru.t1.aayakovlev.tm.api.service.dto.IProjectDTOService;
import ru.t1.aayakovlev.tm.api.service.dto.ITaskDTOService;
import ru.t1.aayakovlev.tm.config.ApplicationConfig;
import ru.t1.aayakovlev.tm.config.DatabaseConfig;
import ru.t1.aayakovlev.tm.config.SecurityConfig;
import ru.t1.aayakovlev.tm.config.WebMVCApplicationConfig;
import ru.t1.aayakovlev.tm.dto.model.TaskDTO;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.util.UserUtil;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

import static ru.t1.aayakovlev.tm.constant.dto.ProjectDTOTestConstant.PROJECT_ONE;
import static ru.t1.aayakovlev.tm.constant.dto.TaskDTOTestConstant.*;
import static ru.t1.aayakovlev.tm.constant.dto.UserDTOTestConstant.USER_LOGIN;
import static ru.t1.aayakovlev.tm.constant.dto.UserDTOTestConstant.USER_PASSWORD;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfig.class, DatabaseConfig.class, SecurityConfig.class, WebMVCApplicationConfig.class})
public class TaskRestEndpointTest {

    @NotNull
    @Resource
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private WebApplicationContext wac;

    @NotNull
    private MockMvc mockMvc;

    @NotNull
    @Autowired
    private IProjectDTOService projectService;

    @NotNull
    @Autowired
    private ITaskDTOService service;

    @NotNull
    private static final String TASKS_URL = "http://localhost:8080/api/tasks";

    @Before
    @SneakyThrows
    public void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USER_LOGIN, USER_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        TASK_ONE.setProjectId(PROJECT_ONE.getId());
        TASK_ONE.setUserId(UserUtil.getUserId());
        TASK_TWO.setProjectId(PROJECT_ONE.getId());
        PROJECT_ONE.setUserId(UserUtil.getUserId());
        projectService.save(PROJECT_ONE);
        add(TASK_ONE);
        add(TASK_TWO);
    }

    @After
    @SneakyThrows
    public void finish() {
        service.deleteAllByUserId(UserUtil.getUserId());
    }

    @SneakyThrows
    private void add(@NotNull final TaskDTO task) {
        @NotNull final String url = TASKS_URL + "/save";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(task);
        mockMvc.perform(MockMvcRequestBuilders.put(url)
                .content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Nullable
    @SneakyThrows
    private TaskDTO findById(@NotNull final String id) {
        @NotNull final String url = TASKS_URL + "/find/" + id;
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print()).andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        if ("".equals(json)) return null;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, TaskDTO.class);
    }

    @Test
    @SneakyThrows
    public void count() {
        @NotNull final String url = TASKS_URL + "/count";
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        Assert.assertEquals(2, objectMapper.readValue(json, Long.class).longValue());
    }

    @Test
    @SneakyThrows
    public void deleteById() {
        @NotNull final String url = TASKS_URL + "/delete/" + TASK_ONE.getId();
        mockMvc.perform(MockMvcRequestBuilders.delete(url)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertThrows(
                NestedServletException.class,
                () -> findById(TASK_ONE.getId())
        );
    }

    @Test
    @SneakyThrows
    public void existsById() {
        @NotNull final String url = TASKS_URL + "/exists/" + TASK_ONE.getId();
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        Assert.assertTrue(objectMapper.readValue(json, Boolean.class));
    }

    @Test
    @SneakyThrows
    public void findAll() {
        @NotNull final String url = TASKS_URL + "/find";
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @Nullable final List<TaskDTO> tasks = Arrays.asList(objectMapper.readValue(json, TaskDTO[].class));
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
    }

    @Test
    @SneakyThrows
    public void findAllByProjectId() {
        @NotNull final String url = TASKS_URL + "/find/project/" + PROJECT_ONE.getId();
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @Nullable final List<TaskDTO> tasks = Arrays.asList(objectMapper.readValue(json, TaskDTO[].class));
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
    }

    @Test
    @SneakyThrows
    public void findById() {
        @NotNull final TaskDTO task = findById(TASK_ONE.getId());
        Assert.assertEquals(TASK_ONE.getId(), task.getId());
        Assert.assertEquals(TASK_ONE.getName(), task.getName());
        Assert.assertEquals(TASK_ONE.getDescription(), task.getDescription());
        Assert.assertEquals(TASK_ONE.getUserId(), task.getUserId());
        Assert.assertEquals(TASK_ONE.getStatus(), task.getStatus());
        Assert.assertEquals(TASK_ONE.getCreated(), task.getCreated());
    }

    @Test
    @SneakyThrows
    public void save() {
        add(TASK_THREE);
        @Nullable final TaskDTO task = findById(TASK_THREE.getId());
        Assert.assertNotNull(task);
    }

    @Test
    @SneakyThrows
    public void update() {
        TASK_TWO.setStatus(Status.IN_PROGRESS);
        @NotNull final String url = TASKS_URL + "/save/" + TASK_TWO.getId();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(TASK_TWO);
        mockMvc.perform(MockMvcRequestBuilders.post(url)
                .content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        @Nullable final TaskDTO task = findById(TASK_TWO.getId());
        Assert.assertEquals(task.getStatus(), Status.IN_PROGRESS);
    }

}
