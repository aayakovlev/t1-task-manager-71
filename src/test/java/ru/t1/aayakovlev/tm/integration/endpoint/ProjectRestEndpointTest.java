package ru.t1.aayakovlev.tm.integration.endpoint;

import ru.t1.aayakovlev.tm.constant.dto.ProjectDTOTestConstant;
import ru.t1.aayakovlev.tm.marker.IntegrationCategory;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.aayakovlev.tm.client.ProjectRestEndpointClient;
import ru.t1.aayakovlev.tm.dto.model.ProjectDTO;
import ru.t1.aayakovlev.tm.exception.entity.ProjectNotFoundException;

import java.util.List;

@Category(IntegrationCategory.class)
public final class ProjectRestEndpointTest {

    @NotNull
    final ProjectRestEndpointClient client = ProjectRestEndpointClient.client();

    @Before
    public void init() {
        client.save(ProjectDTOTestConstant.PROJECT_ONE);
        client.save(ProjectDTOTestConstant.PROJECT_TWO);
        client.save(ProjectDTOTestConstant.PROJECT_THREE);
    }

    @After
    public void finish() {
        client.deleteAll();
    }

    @Test
    @Category(IntegrationCategory.class)
    public void count() {
        client.save(ProjectDTOTestConstant.PROJECT_FOUR);
        long count = client.count();
        Assert.assertEquals(4, count);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void deleteAll() {
        client.deleteAll();
        long count = client.count();
        Assert.assertEquals(0, count);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void deleteById() {
        client.deleteById(ProjectDTOTestConstant.PROJECT_ONE.getId());
        Assert.assertThrows(ProjectNotFoundException.class,
                () -> client.findById(ProjectDTOTestConstant.PROJECT_ONE.getId())
        );
    }

    @Test
    @Category(IntegrationCategory.class)
    public void existsById() {
        final boolean result = client.existsById(ProjectDTOTestConstant.PROJECT_TWO.getId());
        Assert.assertTrue(result);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findAll() {
        @NotNull final List<ProjectDTO> results = client.findAll();
        Assert.assertNotEquals(0, results.size());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findById() {
        @NotNull final ProjectDTO result = client.findById(ProjectDTOTestConstant.PROJECT_TWO.getId());
        Assert.assertEquals(ProjectDTOTestConstant.PROJECT_TWO.getName(), result.getName());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void save() {
        client.save(ProjectDTOTestConstant.PROJECT_FOUR);
        @NotNull final ProjectDTO newProject = client.findById(ProjectDTOTestConstant.PROJECT_FOUR.getId());
        Assert.assertEquals(ProjectDTOTestConstant.PROJECT_FOUR.getName(), newProject.getName());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void update() {
        @NotNull final ProjectDTO existedProject = client.findById(ProjectDTOTestConstant.PROJECT_THREE.getId());
        existedProject.setName("new project name");
        client.update(existedProject);
        @NotNull final ProjectDTO updatedProject = client.findById(ProjectDTOTestConstant.PROJECT_THREE.getId());
        Assert.assertEquals("new project name", updatedProject.getName());
    }

}
