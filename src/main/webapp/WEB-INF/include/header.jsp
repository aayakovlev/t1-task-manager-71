<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }
        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }
        button {
            border: 1px solid black;
            color: black;
            background-color: #bbbbbb;
            padding: 15px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            cursor: pointer;
        }
        .no-btm-margin {
            margin-bottom: 0;
        }

    </style>
    <title>Task manager</title>
</head>
<body>
<table>
    <tr>
        <td style="width: 20%; background-color: white; color: black;">
            <h2 style="margin-bottom: 0px;">TASK MANAGER</h2>
        </td>
        <td style="text-align: right;">
            <sec:authorize access="!isAuthenticated()"><a href="/login">LOGIN</a></sec:authorize>
            <sec:authorize access="isAuthenticated()">
                <a href="/projects">PROJECTS</a> |
                <a href="/tasks">TASKS</a> |
                <sec:authorize access="hasRole('ROLE_ADMINISTRATOR')">
                    <a href="/users">USERS</a> |
                </sec:authorize>
                USER: <sec:authentication property="name"/>
                <a href="/logout">LOGOUT</a>
            </sec:authorize>
        </td>

    </tr>
    <tr>
        <td colspan="2" style="padding-top: 20px;">
