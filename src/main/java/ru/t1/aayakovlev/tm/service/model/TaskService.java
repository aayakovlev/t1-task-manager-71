package ru.t1.aayakovlev.tm.service.model;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aayakovlev.tm.api.service.model.ITaskService;
import ru.t1.aayakovlev.tm.api.service.model.IUserService;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;
import ru.t1.aayakovlev.tm.exception.entity.TaskNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.*;
import ru.t1.aayakovlev.tm.model.Task;
import ru.t1.aayakovlev.tm.model.User;
import ru.t1.aayakovlev.tm.repository.model.TaskRepository;

import java.util.List;
import java.util.Optional;

@Service
public class TaskService implements ITaskService {

    @Getter
    @NotNull
    @Autowired
    private TaskRepository repository;

    @Getter
    @NotNull
    @Autowired
    private IUserService userService;

    @Override
    public long countByUserId(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final User user = userService.findById(userId);
        return getRepository().countByUser(user);
    }

    @Override
    @Transactional
    public void deleteAllByUserId(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final User user = userService.findById(userId);
        getRepository().deleteAllByUser(user);
    }

    @Override
    @Transactional
    public void deleteByUserIdAndId(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (!existsByUserIdAndId(userId, id)) throw new TaskNotFoundException();
        @NotNull final User user = userService.findById(userId);
        getRepository().deleteByUserAndId(user, id);
    }

    @Override
    public boolean existsByUserIdAndId(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final User user = userService.findById(userId);
        return getRepository().existsByUserAndId(user, id);
    }

    @NotNull
    @Override
    public List<Task> findAllByUserId(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final User user = userService.findById(userId);
        return getRepository().findAllByUser(user);
    }

    @NotNull
    @Override
    public List<Task> findAllByUserIdAndProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @NotNull final User user = userService.findById(userId);
        return getRepository().findAllByUserAndProjectId(user, projectId);
    }

    @NotNull
    @Override
    public Task findByUserIdAndId(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final User user = userService.findById(userId);
        @NotNull final Optional<Task> resultEntity = getRepository().findByUserAndId(user, id);
        if (!resultEntity.isPresent()) throw new TaskNotFoundException();
        return resultEntity.get();
    }

    @NotNull
    @Override
    public Task save(@Nullable final Task task) throws EntityEmptyException {
        if (task == null) throw new EntityEmptyException();
        return getRepository().save(task);
    }

}
