package ru.t1.aayakovlev.tm.service.model;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aayakovlev.tm.api.service.model.IProjectService;
import ru.t1.aayakovlev.tm.api.service.model.IUserService;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;
import ru.t1.aayakovlev.tm.exception.entity.ProjectNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.IdEmptyException;
import ru.t1.aayakovlev.tm.exception.field.UserIdEmptyException;
import ru.t1.aayakovlev.tm.model.Project;
import ru.t1.aayakovlev.tm.model.User;
import ru.t1.aayakovlev.tm.repository.model.ProjectRepository;

import java.util.List;
import java.util.Optional;

@Service
public class ProjectService implements IProjectService {

    @Getter
    @NotNull
    @Autowired
    private ProjectRepository repository;

    @Getter
    @NotNull
    @Autowired
    private IUserService userService;

    @Override
    public long countByUserId(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final User user = userService.findById(userId);
        return getRepository().countByUser(user);
    }

    @Override
    @Transactional
    public void deleteAllByUserId(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final User user = userService.findById(userId);
        getRepository().deleteAllByUser(user);
    }

    @Override
    @Transactional
    public void deleteByUserIdAndId(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (!existsByUserIdAndId(userId, id)) throw new ProjectNotFoundException();
        @NotNull final User user = userService.findById(userId);
        getRepository().deleteByUserAndId(user, id);
    }

    @Override
    public boolean existsByUserIdAndId(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final User user = userService.findById(userId);
        return getRepository().existsByUserAndId(user, id);
    }

    @NotNull
    @Override
    public List<Project> findAllByUserId(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final User user = userService.findById(userId);
        return getRepository().findAllByUser(user);
    }

    @NotNull
    @Override
    public Project findByUserIdAndId(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final User user = userService.findById(userId);
        @NotNull final Optional<Project> resultEntity = getRepository().findByUserAndId(user, id);
        if (!resultEntity.isPresent()) throw new ProjectNotFoundException();
        return resultEntity.get();
    }

    @NotNull
    @Override
    @Transactional
    public Project save(@Nullable final Project project) throws EntityEmptyException {
        if (project == null) throw new EntityEmptyException();
        return getRepository().save(project);
    }

}
