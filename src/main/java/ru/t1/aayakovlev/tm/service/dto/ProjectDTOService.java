package ru.t1.aayakovlev.tm.service.dto;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aayakovlev.tm.api.service.dto.IProjectDTOService;
import ru.t1.aayakovlev.tm.dto.model.ProjectDTO;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;
import ru.t1.aayakovlev.tm.exception.entity.ProjectNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.IdEmptyException;
import ru.t1.aayakovlev.tm.exception.field.UserIdEmptyException;
import ru.t1.aayakovlev.tm.repository.dto.ProjectDTORepository;

import java.util.List;
import java.util.Optional;

@Service
public class ProjectDTOService implements IProjectDTOService {

    @Getter
    @NotNull
    @Autowired
    private ProjectDTORepository repository;

    @Override
    public long countByUserId(@Nullable final String userId) {
        return getRepository().countByUserId(userId);
    }

    @Override
    @Transactional
    public void deleteAllByUserId(@Nullable final String userId) {
        getRepository().deleteAllByUserId(userId);
    }

    @Override
    @Transactional
    public void deleteByUserIdAndId(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (!existsByUserIdAndId(userId, id)) throw new ProjectNotFoundException();
        getRepository().deleteByUserIdAndId(userId, id);
    }

    @Override
    public boolean existsByUserIdAndId(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return getRepository().existsByUserIdAndId(userId, id);
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAllByUserId(@Nullable final String userId) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return getRepository().findAllByUserId(userId);
    }

    @NotNull
    @Override
    public ProjectDTO findByUserIdAndId(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final Optional<ProjectDTO> resultEntity = getRepository().findByUserIdAndId(userId, id);
        if (!resultEntity.isPresent()) throw new ProjectNotFoundException();
        return resultEntity.get();
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO save(@Nullable final ProjectDTO project) throws EntityEmptyException {
        if (project == null) throw new EntityEmptyException();
        return getRepository().save(project);
    }

}
