package ru.t1.aayakovlev.tm.dto.soap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@NoArgsConstructor
@XmlType(name = "")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "userDeleteByIdRequest")
public class UserDeleteByIdRequest {

    @XmlElement(required = true)
    protected String id;

    public UserDeleteByIdRequest(@NotNull final String id) {
        this.id = id;
    }

}
