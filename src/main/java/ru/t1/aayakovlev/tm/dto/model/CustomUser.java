package ru.t1.aayakovlev.tm.dto.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

@Setter
@Getter
public final class CustomUser extends User {

    @NotNull
    private String userId;

    public CustomUser withUserId(@NotNull final String userId) {
        this.userId = userId;
        return this;
    }
    public CustomUser(@NotNull final UserDetails user) {
        super(
                user.getUsername(),
                user.getPassword(),
                user.isEnabled(),
                user.isAccountNonExpired(),
                user.isCredentialsNonExpired(),
                user.isAccountNonExpired(),
                user.getAuthorities()
        );
    }

}
