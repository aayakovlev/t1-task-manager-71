package ru.t1.aayakovlev.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.aayakovlev.tm.api.service.dto.IProjectDTOService;
import ru.t1.aayakovlev.tm.api.service.dto.ITaskDTOService;
import ru.t1.aayakovlev.tm.dto.model.CustomUser;
import ru.t1.aayakovlev.tm.dto.model.TaskDTO;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;

@Controller
@RequestMapping("/tasks")
public class TaskController {

    @Autowired
    private ITaskDTOService service;

    @Autowired
    private IProjectDTOService projectService;

    @GetMapping("")
    @Secured({"ROLE_USER", "ROLE_ADMINISTRATOR"})
    public ModelAndView index(
            @AuthenticationPrincipal @NotNull final CustomUser user
    ) throws AbstractException {
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-list");
        modelAndView.addObject("tasks", service.findAllByUserId(user.getUserId()));
        modelAndView.addObject("projectService", projectService);
        return modelAndView;
    }

    @GetMapping("/create")
    @Secured({"ROLE_USER", "ROLE_ADMINISTRATOR"})
    public String create(
            @AuthenticationPrincipal @NotNull final CustomUser user
    ) throws EntityEmptyException {
        @NotNull final TaskDTO task = new TaskDTO("new Task " + System.currentTimeMillis());
        task.setUserId(user.getUserId());
        service.save(task);
        return "redirect:/tasks";
    }

    @GetMapping("/delete/{id}")
    @Secured({"ROLE_USER", "ROLE_ADMINISTRATOR"})
    public String delete(
            @AuthenticationPrincipal @NotNull final CustomUser user,
            @PathVariable("id") @Nullable final String id
    ) throws AbstractException {
        service.deleteByUserIdAndId(user.getUserId(), id);
        return "redirect:/tasks";
    }

    @PostMapping("/edit/{id}")
    @Secured({"ROLE_USER", "ROLE_ADMINISTRATOR"})
    public String edit(
            @AuthenticationPrincipal @NotNull final CustomUser user,
            @ModelAttribute("task") @NotNull final TaskDTO task,
            @NotNull final BindingResult result
    ) throws EntityEmptyException {
        if (task.getProjectId() == null || task.getProjectId().isEmpty()) task.setProjectId(null);
        task.setUserId(user.getUserId());
        service.save(task);
        return "redirect:/tasks";
    }

    @GetMapping("/edit/{id}")
    @Secured({"ROLE_USER", "ROLE_ADMINISTRATOR"})
    public ModelAndView edit(
            @AuthenticationPrincipal @NotNull final CustomUser user,
            @PathVariable("id") @Nullable final String id
    ) throws AbstractException {
        @NotNull final TaskDTO task = service.findByUserIdAndId(user.getUserId(), id);
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-edit");
        modelAndView.addObject("task", task);
        modelAndView.addObject("projects", projectService.findAllByUserId(user.getUserId()));
        modelAndView.addObject("statuses", Status.values());
        return modelAndView;
    }

}
