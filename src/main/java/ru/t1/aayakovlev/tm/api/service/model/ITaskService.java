package ru.t1.aayakovlev.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;
import ru.t1.aayakovlev.tm.exception.field.UserIdEmptyException;
import ru.t1.aayakovlev.tm.model.Task;

import java.util.List;

public interface ITaskService {

    long countByUserId(@Nullable final String userId) throws AbstractException;

    @Transactional
    void deleteAllByUserId(@Nullable final String userId) throws AbstractException;

    @Transactional
    void deleteByUserIdAndId(@Nullable final String userId, @Nullable final String id) throws AbstractException;

    boolean existsByUserIdAndId(@Nullable final String userId, @Nullable final String id) throws AbstractException;

    @NotNull
    List<Task> findAllByUserId(@Nullable final String userId) throws AbstractException;

    @NotNull
    List<Task> findAllByUserIdAndProjectId(@Nullable final String userId, @Nullable final String projectId) throws AbstractException;

    @NotNull
    Task findByUserIdAndId(@Nullable final String userId, @Nullable final String id) throws AbstractException;

    @NotNull
    @Transactional
    Task save(@Nullable final Task task) throws EntityEmptyException;

}
