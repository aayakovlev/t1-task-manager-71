package ru.t1.aayakovlev.tm.exception.field;

import ru.t1.aayakovlev.tm.exception.field.AbstractFieldException;

public final class UserIdEmptyException extends AbstractFieldException {

    public UserIdEmptyException() {
        super("Error! User id is empty...");
    }

}
