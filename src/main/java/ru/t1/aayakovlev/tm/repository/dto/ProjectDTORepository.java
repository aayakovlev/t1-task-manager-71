package ru.t1.aayakovlev.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.aayakovlev.tm.dto.model.ProjectDTO;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public interface ProjectDTORepository extends JpaRepository<ProjectDTO, String> {

    long countByUserId(@Nullable final String userId);

    void deleteAllByUserId(@Nullable final String userId);

    void deleteByUserIdAndId(@Nullable final String userId, @Nullable final String id);

    boolean existsByUserIdAndId(@Nullable final String userId, @Nullable final String id);

    @NotNull
    List<ProjectDTO> findAllByUserId(@Nullable final String userId);

    @NotNull
    Optional<ProjectDTO> findByUserIdAndId(@Nullable final String userId, @Nullable final String id);

}
