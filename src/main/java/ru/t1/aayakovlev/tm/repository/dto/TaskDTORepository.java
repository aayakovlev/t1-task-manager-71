package ru.t1.aayakovlev.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.aayakovlev.tm.dto.model.TaskDTO;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public interface TaskDTORepository extends JpaRepository<TaskDTO, String> {

    long countByUserId(@Nullable final String userId);

    void deleteAllByUserId(@Nullable final String userId);

    void deleteAllByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId);

    void deleteByUserIdAndId(@Nullable final String userId, @Nullable final String id);

    boolean existsByUserIdAndId(@Nullable final String userId, @Nullable final String id);

    @NotNull
    List<TaskDTO> findAllByUserId(@Nullable final String userId);

    @NotNull
    List<TaskDTO> findAllByUserIdAndProjectId(@Nullable final String userId, @Nullable final String projectId);

    @NotNull
    Optional<TaskDTO> findByUserIdAndId(@Nullable final String userId, @Nullable final String id);

}
