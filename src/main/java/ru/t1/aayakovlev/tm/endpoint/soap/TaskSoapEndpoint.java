package ru.t1.aayakovlev.tm.endpoint.soap;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.aayakovlev.tm.api.endpoint.soap.ITaskSoapEndpoint;
import ru.t1.aayakovlev.tm.api.service.dto.ITaskDTOService;
import ru.t1.aayakovlev.tm.dto.model.TaskDTO;
import ru.t1.aayakovlev.tm.dto.soap.*;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;
import ru.t1.aayakovlev.tm.util.UserUtil;

@Endpoint
public final class TaskSoapEndpoint implements ITaskSoapEndpoint {

    public final static String LOCATION_URI = "/ws";

    public final static String PORT_TYPE_NAME = "TaskSoapEndpointPort";

    public final static String NAMESPACE = "http://aayakovlev.t1.ru/tm/dto/soap";

    @NotNull
    @Autowired
    private ITaskDTOService service;

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "taskCountRequest", namespace = NAMESPACE)
    public TaskCountResponse count(
            @RequestPayload @NotNull final TaskCountRequest request
    ) throws AbstractException {
        return new TaskCountResponse(service.countByUserId(UserUtil.getUserId()));
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteAllRequest", namespace = NAMESPACE)
    public TaskDeleteAllResponse deleteAll(
            @RequestPayload @NotNull final TaskDeleteAllRequest request
    ) throws AbstractException {
        service.deleteAllByUserId(UserUtil.getUserId());
        return new TaskDeleteAllResponse();
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteByIdRequest", namespace = NAMESPACE)
    public TaskDeleteByIdResponse deleteById(
            @RequestPayload @NotNull final TaskDeleteByIdRequest request
    ) throws AbstractException {
        service.deleteByUserIdAndId(UserUtil.getUserId(), request.getId());
        return new TaskDeleteByIdResponse();
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "taskExistsByIdRequest", namespace = NAMESPACE)
    public TaskExistsByIdResponse existsById(
            @RequestPayload @NotNull final TaskExistsByIdRequest request
    ) throws AbstractException {
        return new TaskExistsByIdResponse(service.existsByUserIdAndId(UserUtil.getUserId(), request.getId()));
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "taskFindAllRequest", namespace = NAMESPACE)
    public TaskFindAllResponse findAll(
            @RequestPayload @NotNull final TaskFindAllRequest request
    ) throws AbstractException {
        return new TaskFindAllResponse(service.findAllByUserId(UserUtil.getUserId()));
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "taskFindAllByProjectIdRequest", namespace = NAMESPACE)
    public TaskFindAllByProjectIdResponse findAll(
            @RequestPayload @NotNull final TaskFindAllByProjectIdRequest request
    ) throws AbstractException {
        return new TaskFindAllByProjectIdResponse(service.findAllByUserIdAndProjectId(UserUtil.getUserId(), request.getProjectId()));
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "taskFindByIdRequest", namespace = NAMESPACE)
    public TaskFindByIdResponse findById(
            @RequestPayload @NotNull final TaskFindByIdRequest request
    ) throws AbstractException {
        return new TaskFindByIdResponse(service.findByUserIdAndId(UserUtil.getUserId(), request.getId()));
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "taskSaveRequest", namespace = NAMESPACE)
    public TaskSaveResponse save(
            @RequestPayload @NotNull final TaskSaveRequest request
    ) throws EntityEmptyException {
        @NotNull final TaskDTO task = request.getTask();
        task.setUserId(UserUtil.getUserId());
        return new TaskSaveResponse(service.save(task));
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "taskUpdateRequest", namespace = NAMESPACE)
    public TaskUpdateResponse update(
            @RequestPayload @NotNull final TaskUpdateRequest request
    ) throws EntityEmptyException {
        @NotNull final TaskDTO task = request.getTask();
        task.setUserId(UserUtil.getUserId());
        return new TaskUpdateResponse(service.save(task));
    }

}
